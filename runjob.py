#!/usr/bin/env python3
import argparse
import sys

import jobs


def main(args) -> None:
    all_jobs = jobs.list_all()
    parser = create_parser(all_jobs)
    opts = parser.parse_args(args)
    for job in all_jobs:
        if job.cmd == opts.subcmd:
            return job.run(vars(opts))


def create_parser(jobs) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    action = parser.add_subparsers(dest='subcmd', title='available jobs')
    for job in jobs:
        subparser = action.add_parser(job.cmd)
        job.build_parser(subparser)
    return parser


if __name__ == '__main__':
    main(sys.argv[1:] or ['--help'])
