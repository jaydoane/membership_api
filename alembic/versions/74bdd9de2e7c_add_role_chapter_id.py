"""Add Role.chapter_id

Revision ID: 74bdd9de2e7c
Revises: 9b88e2749971
Create Date: 2018-08-12 21:00:07.580832

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from config import DEFAULT_CHAPTER_ID, DEFAULT_CHAPTER_NAME

# revision identifiers, used by Alembic.
revision = '74bdd9de2e7c'
down_revision = '9b88e2749971'
branch_labels = None
depends_on = None


def upgrade():
    # Add the default chapter to chapter table
    op.execute(
        "INSERT IGNORE INTO chapters (id, name) VALUES "
        f"({DEFAULT_CHAPTER_ID}, '{DEFAULT_CHAPTER_NAME}')"
    )
    # Add the chapter_id column
    op.execute(
        "ALTER TABLE roles ADD COLUMN chapter_id INTEGER NOT NULL AFTER `id`"
    )
    # Add the default chapter id to all existing roles
    op.execute(f"UPDATE roles SET chapter_id={DEFAULT_CHAPTER_ID}")
    # Add the non-NULL constraint
    op.alter_column(
        'roles', 'chapter_id',
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    # Add the foreign key constraint
    op.create_foreign_key(None, 'roles', 'chapters', ['chapter_id'], ['id'])


def downgrade():
    op.drop_constraint(None, 'roles', type_='foreignkey')
    op.drop_column('roles', 'chapter_id')
    op.execute(
        f"DELETE FROM chapters WHERE id={DEFAULT_CHAPTER_ID} AND name='{DEFAULT_CHAPTER_NAME}'"
    )
