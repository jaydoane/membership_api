"""Add unique index on roles.

Revision ID: c00a5a9358f1
Revises: 7e922d21e23e
Create Date: 2018-02-14 21:22:53.102575

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'c00a5a9358f1'
down_revision = '7e922d21e23e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint(None, 'roles', ['member_id', 'committee_id', 'role'])


def downgrade():
    op.drop_constraint(None, 'roles', type_='unique')
