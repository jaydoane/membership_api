import argparse
import importlib
import pkgutil
from typing import List, Optional


class Job:
    cmd: str
    description: Optional[str]

    def __init__(self):
        # by default the name of the job is the module name to where it is located
        if not getattr(self, 'name', None):
            self.cmd = self.__module__.split('.', maxsplit=1)[1]

    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        pass

    def run(self, config: dict) -> None:
        pass


def list_all() -> List[Job]:
    for _, name, _ in pkgutil.iter_modules([__name__]):
        importlib.import_module('.' + name, __name__)
    return [j() for j in Job.__subclasses__()]
