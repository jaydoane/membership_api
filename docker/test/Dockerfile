FROM alpine:3.6

LABEL maintainer "tech@dsasf.org"

# Requires running with the context from $DSASF_HOME/membership_api (see README.md for settings):
#     docker build . -f docker/migrate/Dockerfile

WORKDIR /usr/src/app

RUN apk add --no-cache \
        build-base \
        mariadb-dev \
        python3 \
        python3-dev

COPY requirements.txt ./
COPY requirements-dev.txt ./

RUN pip3 install --no-cache-dir -r requirements-dev.txt

COPY config ./config
COPY membership ./membership
COPY jobs ./jobs
COPY flask_app.py ./
COPY tests ./tests
COPY pytest.ini ./
COPY setup.cfg ./
COPY example.env ./.env

ENTRYPOINT ["py.test"]
