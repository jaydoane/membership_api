import json
import logging
from typing import Dict

import pkg_resources
import requests

import membership
from config import EMAIL_API_KEY, EMAIL_DOMAIN, MAILGUN_URL, USE_EMAIL, PORTAL_URL, \
    SUPPORT_EMAIL_ADDRESS
from membership.database.models import Email, Member


def send_emails(
        sender: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]]) -> None:
    """
    Create an email campaign from the sender using the given MailGun email template formatted string
    and substitute the recipient variables for every email key in the dict.

    NOTE: If `recipient_variables` is empty, then the email is sent to nobody, otherwise the email
    is sent to every key of the dictionary given.

    :param sender: the from field of the email to be received
    :param subject: the subject line of the email
    :param email_template: the full template with all variables / recipient variables declared
    :param recipient_variables: all the recipients of this email by email address with
                                all variables associated with this address in the values
    """
    url = f'https://api.mailgun.net/v3/{EMAIL_DOMAIN}/messages'
    filtered_recipient_variables = {
        member.email_address: obj for member, obj in recipient_variables.items()
        if not member.do_not_email
    }
    payload = [
        ('from', sender),
        ('recipient-variables', json.dumps(filtered_recipient_variables)),
        ('subject', subject),
        ('html', email_template)
    ]
    payload.extend([('to', email) for email in filtered_recipient_variables.keys()])
    if USE_EMAIL:
        r = requests.post(url, data=payload, auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            logging.error(r.text)


def send_welcome_email(member: Member, verify_url: str) -> None:
    # send a welcome email with the email verification / password reset link from Auth0
    sender = f"DSA SF Tech Committee <{SUPPORT_EMAIL_ADDRESS}>"
    template = pkg_resources.resource_string(membership.__name__, 'templates/welcome_email.html') \
        .decode("utf-8") \
        .format(PORTAL_URL=PORTAL_URL)

    recipient_variables = {member: {'name': member.first_name, 'link': verify_url}}
    send_emails(
        sender,
        'Welcome to the DSA SF Membership Portal, %recipient.name%!',
        template,
        recipient_variables,
    )


def update_email(email: Email) -> str:
    """
    Create or update routes in Mailgun
    :param email: the incoming email address (should have a least one forwarding address)
    :return: the external_id from mailgun
    """
    payload = [
        ('priority', 0),
        ('description', 'Forwarding rule for {address}'.format(address=email.email_address)),
        ('expression', 'match_recipient("{address}")'.format(address=email.email_address)),
    ]
    payload.extend([('action', 'forward("{address}")'.format(address=forward.forward_to))
                    for forward in email.forwarding_addresses])
    if email.external_id:
        r = requests.put(MAILGUN_URL + '/' + email.external_id,
                         data=payload,
                         auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            raise Exception('Mailgun api failed to update email.')
        return email.external_id
    else:
        r = requests.post(MAILGUN_URL, data=payload, auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            raise Exception('Mailgun api failed to create email.')
        return r.json().get('route').get('id')


def remove_email(external_id: str) -> None:
    r = requests.delete(MAILGUN_URL + '/' + external_id, auth=('api', EMAIL_API_KEY))
    if r.status_code > 299:
        raise Exception('Mailgun api failed to delete email.')
