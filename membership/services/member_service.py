from typing import Optional, List
from membership.database.base import Session
from membership.database.models import Member
from membership.models import MemberQueryResult
from membership.services import AttendeeService
from sqlalchemy import or_


class MemberService:
    def __init__(self, attendee_service: AttendeeService):
        self.attendee_service = attendee_service

    def find_by_email(self, email: str, session: Session) -> Optional[Member]:
        return session.query(Member) \
                      .filter_by(normalized_email=Member.normalize_email(email)) \
                      .one_or_none()

    def all(self, session: Session) -> MemberQueryResult:
        members = self._all_members(session)
        next_cursor = self._get_cursor_from_members(members, last_cursor=None)
        return self._as_eligible_members_result(members, next_cursor, False, session)

    def query(self,
              session: Session,
              cursor: Optional[str] = None,
              page_size: Optional[int] = None,
              query_str: Optional[str] = None) -> MemberQueryResult:

        min_id = int(cursor) + 1 if cursor else 0

        # Query for 1 more so we know if we have more
        limit = page_size + 1 if page_size else None
        members = self._query_members(session=session,
                                      min_id=min_id,
                                      limit=limit,
                                      query_str=query_str)

        has_more = False
        if page_size is not None and len(members) == page_size + 1:
            has_more = True
            members = members[:-1]  # remove extra element

        next_cursor = self._get_cursor_from_members(members, last_cursor=cursor)
        return self._as_eligible_members_result(members, next_cursor, has_more, session)

    def _all_members(self, session: Session) -> List[Member]:
        query = session \
                .query(Member) \
                .all()

        return list(query)

    def _query_members(self,
                       session: Session,
                       min_id: int,
                       limit: Optional[int] = None,
                       query_str: Optional[str] = None) -> List[Member]:
        members_query = session.query(Member) \
                               .order_by(Member.id) \
                               .filter(Member.id >= min_id)

        if query_str is not None:
            query_words = query_str.split()
            for query_word in query_words:
                formatted_query = query_word + '%'
                # Case-insensitive query for string in user name and email
                or_filter = or_(Member.first_name.ilike(formatted_query),
                                Member.last_name.ilike(formatted_query),
                                Member.email_address.ilike(formatted_query))
                members_query = members_query.filter(or_filter)
        if limit:
            members_query = members_query.limit(limit)
        result = list(members_query.all())
        for member in result:
            assert member.id >= int(min_id)
        return result

    def _as_eligible_members_result(self, members: List[Member],
                                    next_cursor: str,
                                    has_more: bool,
                                    session: Session) -> MemberQueryResult:
        members_with_eligibility = self.attendee_service.members_as_eligible(session, members)

        return MemberQueryResult(
            members=members_with_eligibility,
            cursor=next_cursor,
            has_more=has_more,
        )

    def _get_cursor_from_members(self, members: List[Member], last_cursor: Optional[str]) -> str:
        if len(members) > 0:
            return str(members[-1].id)
        elif last_cursor:
            return last_cursor
        else:
            return str(-1)
