from datetime import datetime
from typing import Optional

from membership.database.models import EmailTemplate
from membership.models.authz import AuthContext
from membership.schemas import JsonObj
from membership.schemas.rest.email_templates import EmailTemplateRest


class EmailTemplateService:

    @classmethod
    def upsert(
            cls,
            ctx: AuthContext,
            json: Optional[JsonObj],
            existing: Optional[EmailTemplate] = None) -> EmailTemplate:
        template = existing or EmailTemplate()
        EmailTemplateRest.update(template, json or {}, validate=existing is None)
        template.last_updated = datetime.now()
        ctx.session.add(template)
        ctx.session.commit()
        return template

    @classmethod
    def delete(cls, ctx: AuthContext, id: int):
        found_template = ctx.session.query(EmailTemplate).get(id)
        if found_template:
            ctx.session.delete(found_template)
            ctx.session.commit()
        return found_template is not None
