from typing import List

from membership.database.models import Interest, InterestTopic, Member
from membership.models.authz import AuthContext


class InterestTopicsService:

    @classmethod
    def create_interests(
            cls,
            ctx: AuthContext,
            member: Member,
            topic_names: List[str]) -> List[Interest]:
        topics: List[InterestTopic] = []
        for topic_name in topic_names:
            topic: Optional[InterestTopic] = ctx.session \
                    .query(InterestTopic) \
                    .filter(InterestTopic.name == topic_name) \
                    .one_or_none()
            if topic is None:
                topic = InterestTopic(name=topic_name)
                ctx.session.add(topic)
            topics.append(topic)

        interests: List[Interest] = []
        member_interest_query = ctx.session.query(Interest).filter(Interest.member == member)
        for topic in topics:
            interest: Optional[Interest] = member_interest_query \
                                                .filter(Interest.topic == topic) \
                                                .one_or_none()
            if interest is None:
                interest: Interest = Interest(member_id=member.id, topic_id=topic.id)
                ctx.session.add(interest)
            interests.append(interest)

        ctx.session.commit()
        return interests

    @classmethod
    def list_member_interest_topics(
            cls,
            ctx: AuthContext,
            member: Member) -> List[Interest]:
        interests: List[Interest] = ctx.session \
                .query(Interest).filter(Interest.member == member).all()
        return [i.topic for i in interests]

    @classmethod
    def list_all_interest_topics(
            cls,
            ctx: AuthContext) -> List[InterestTopic]:
        return ctx.session.query(InterestTopic).all()
