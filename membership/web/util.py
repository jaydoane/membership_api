import json
import logging
from datetime import date, datetime
from decimal import Decimal
from typing import Optional

from flask import Response, request
from flask.json import JSONEncoder

from membership.schemas import Json

logger = logging.getLogger(__name__)


class Ok(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Ok, self).__init__(
            body, status=200, mimetype=mimetype)


class NotFound(Response):
    def __init__(self, err: str = 'Not Found') -> None:
        payload = {'status': 'failed', 'err': err}
        super(NotFound, self).__init__(
            json.dumps(payload), status=404, mimetype='application/json')


class BadRequest(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(BadRequest, self).__init__(
            json.dumps(payload), status=400, mimetype='application/json')


class ServerError(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServerError, self).__init__(
            json.dumps(payload), status=500, mimetype='application/json')


def requires_json(f):
    def decorated(*args, **kwargs):
        if not request.is_json:
            return BadRequest("Expected 'Content-Type:application/json' header")
        else:
            return f(*args, **kwargs)
    # Blueprint methods have to have unique names
    decorated.__name__ = f.__name__
    return decorated


# TODO: We should always convert datetime to a number of millis from epoch
class CustomEncoder(JSONEncoder):
    """ Custom encoder class converts Decimals to strings and datetime objects into ISO
    formatted strings. """

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)
