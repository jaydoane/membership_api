from flask import Blueprint, Response, request

from sqlalchemy.exc import IntegrityError

from membership.database.models import Member, Meeting
from membership.models import AuthContext
from membership.schemas.rest import EmailTopicsRest
from membership.services import AttendeeService, InterestTopicsService, MemberService
from membership.repos import MeetingRepo
from membership.web.auth import requires_auth
from membership.web.util import Ok, BadRequest, requires_json

email_topics_api = Blueprint('email_topics_api', __name__)
interest_topics_service = InterestTopicsService
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService(meetings=meeting_repository)
member_service = MemberService(attendee_service)


@email_topics_api.route('/email_topics', methods=['GET'])
@requires_auth(admin=True)
def list_email_topics(ctx: AuthContext) -> Response:
    return Ok([
        EmailTopicsRest.format(topic)
        for topic in interest_topics_service.list_all_interest_topics(ctx)
    ])


@email_topics_api.route('/email_topics/add_member', methods=['POST'])
@requires_auth(admin=True)
@requires_json
def add_member_to_topics(ctx: AuthContext) -> Response:
    # TODO: Require auth token specifically granted to the Wordpress form
    email_address: str = request.json.get('email', '').strip()
    first_name: str = request.json.get('first_name', '').strip()
    last_name: str = request.json.get('last_name', '').strip()

    if email_address is None or len(email_address) == 0:
        return BadRequest('Missing required parameter "email"')

    member = member_service.find_by_email(email_address, ctx.session)
    if member is None:
        # TODO: Centralize member creation in MemberService (this code is duplicated in members.py)
        member = Member(first_name=first_name, last_name=last_name, email_address=email_address)
        ctx.session.add(member)
        try:
            ctx.session.flush()
        except IntegrityError:
            ctx.session.rollback()
            return BadRequest(f"Failed to create member with address: {email_address}")

    topics_str = request.json.get('topics', '')
    topics = [t.strip() for t in topics_str.split(',') if t.strip() is not '']
    if len(topics) > 0:
        interest_topics_service.create_interests(ctx, member, topics)

    ctx.session.commit()
    return Ok()
