from typing import Optional  # NOQA: F401

from flask import Blueprint, jsonify, make_response, request

from membership.database.models import Email, ForwardingAddress
from membership.models import AuthContext
from membership.schemas.rest.emails import format_email
from membership.util.email import remove_email, update_email
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, Ok

email_api = Blueprint('email_api', __name__)


@email_api.route('/emails', methods=['GET'])
@requires_auth(admin=True)
def list_email_addresses(ctx: AuthContext):
    emails = ctx.session.query(Email).all()
    results = [format_email(e) for e in emails]
    return jsonify(results)


@email_api.route('/emails', methods=['POST'])
@requires_auth(admin=True)
def create_email_address(ctx: AuthContext):
    email_address = request.json['email_address']
    if ctx.session.query(Email.id).filter_by(email_address=email_address).one_or_none():
        return BadRequest('Email address already exists')
    email = Email()
    email.email_address = request.json['email_address']
    for forwarding_email in request.json['forwarding_addresses']:
        forwarding_address = ForwardingAddress()
        forwarding_address.incoming_email = email
        forwarding_address.forward_to = forwarding_email
    email.external_id = update_email(email)
    ctx.session.add(email)
    ctx.session.commit()
    return make_response(jsonify({'status': 'success', 'email': format_email(email)}), 201)


@email_api.route('/emails/<int:email_id>', methods=['PUT'])
@requires_auth(admin=True)
def update_email_address(ctx: AuthContext, email_id: int):
    email = ctx.session.query(Email).get(email_id)  # type: Email
    if not email:
        return NotFound('No email with that id')
    if 'email_address' in request.json:
        email.email_address = request.json['email_address']
    to_add = set(request.json['forwarding_addresses'])
    for forwarding_address in list(email.forwarding_addresses):
        if forwarding_address.forward_to in to_add:
            # no need to alter existing email addresses
            to_add.remove(forwarding_address.forward_to)
        else:
            # remove email addresses not in the new set
            email.forwarding_addresses.remove(forwarding_address)
            ctx.session.delete(forwarding_address)
    # add new email addresses
    for forwarding_email in to_add:
        forwarding_address = ForwardingAddress()
        forwarding_address.incoming_email = email
        forwarding_address.forward_to = forwarding_email
        ctx.session.add(forwarding_address)

    email.external_id = update_email(email)
    ctx.session.commit()
    return jsonify({'status': 'success', 'email': format_email(email)})


@email_api.route('/emails/<int:email_id>', methods=['DELETE'])
@requires_auth(admin=True)
def delete_email_address(ctx: AuthContext, email_id: int):
    email: Optional[Email] = ctx.session.query(Email).get(email_id)
    if not email:
        return NotFound('No email with that id')
    external_id = email.external_id
    ctx.session.delete(email)

    remove_email(external_id)
    ctx.session.commit()
    return Ok()
