from membership.database.models import Committee
from membership.schemas import JsonObj


def format_committee(committee: Committee) -> JsonObj:
    return {
        'id': committee.id,
        'name': committee.name
    }
