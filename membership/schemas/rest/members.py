from typing import Callable, Iterable, List

from membership.database.models import Member, Role
from membership.models import Authorization, EligibleMember, MemberQueryResult
from membership.schemas import JsonObj
from membership.schemas.rest.generic import format_iterable


def format_eligible_member(eligible_member: EligibleMember) -> JsonObj:
    return {
        'id': eligible_member.id,
        'name': eligible_member.name,
        'email': eligible_member.email_address,  # TODO: Require admin access to get email address
        'eligibility': {
            'is_eligible': eligible_member.is_eligible,
            'message': eligible_member.message,
        },
    }


format_eligible_member_list: Callable[[Iterable[EligibleMember]], List[JsonObj]] = \
    format_iterable(format_eligible_member)


def format_role(role: Role) -> JsonObj:
    return {
        'role': role.role,
        'committee': role.committee.name if role.committee else 'general',
    }


def format_member_query_result(member_query_result: MemberQueryResult) -> JsonObj:
    return {
        'members': format_eligible_member_list(member_query_result.members),
        'cursor': member_query_result.cursor,
        'has_more': member_query_result.has_more,
    }


def format_member_info(member: Member, az: Authorization) -> JsonObj:
    return {
        'first_name': member.first_name,
        'last_name': member.last_name,
        'biography': member.biography,
        **(
            {'email_address': member.email_address}
            if az.has_role('admin', member_id=member.id)
            else {}
        )
    }


def format_member_basics(member: Member, az: Authorization) -> JsonObj:
    return {
        'id': member.id,
        'info': format_member_info(member, az),
        'roles': [format_role(role) for role in member.roles],
    }


def format_member_full(member: Member, az: Authorization) -> JsonObj:
    return {
        **format_member_basics(member, az),
        'meetings': [attendee.meeting.name for attendee in member.meetings_attended],
        'votes': [
            {
                'election_id': eligible_vote.election_id,
                'election_name': eligible_vote.election.name,
                'election_status': eligible_vote.election.status,
                'voted': eligible_vote.voted,
            } for eligible_vote in member.eligible_votes
        ],
    }
