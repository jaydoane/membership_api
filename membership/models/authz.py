from typing import Any, Iterable, Optional, Set  # NOQA: F401

from membership.database.base import Session
from membership.database.models import Member


class AzRole:
    """
    Authorization model equivalent of the database model `Role`.

    Note: This has nothing to do with Auth0 except that this data is derived from the member with
    the email matching the token from the request.
    """

    def __init__(self, name: str, committee_id: Optional[int] = None):
        self.name = name
        self.committee_id = committee_id

    def __eq__(self, other: Any) -> bool:
        return self.name == other.name and self.committee_id == other.committee_id

    def __hash__(self) -> int:
        return hash((self.name, self.committee_id))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(" \
               f"name={repr(self.name)}, " \
               f"committee_id={repr(self.committee_id)})"


class Authorization:
    """
    Authorization information of the requester as extracted from the request token and database.

    Note: This has nothing to do with Auth0 except that this data is derived from the member with
    the email matching the token from the request.
    """

    def __init__(self, member_id: int, roles: Iterable[AzRole]):
        if member_id is None:
            raise ValueError('member_id cannot be None')
        self.member_id: int = int(member_id)
        self.roles: Set[AzRole] = set(roles)

    def has_role(
            self,
            role_name: str,
            committee_id: Optional[int] = None,
            member_id: Optional[int] = None) -> bool:
        """
        Determines if the authorized user has the given role.

        :param role_name: the name of the role to lookup
        :param committee_id: the committee_id of the role, any roles not matching this committee_id
                             are excluded from the lookup
        :param member_id: the member.id that the data belongs to; if provided, then the role lookup
                          will short-circuit to True if the authorized requester has the same id as
                          the given member_id
        :return: True if the authorized requester has the given role for the specified committee
                 and or member
        """
        return self.member_id == member_id or any(
            role.name == role_name for role in self.roles if role.committee_id == committee_id
        )

    def __eq__(self, other: Any) -> bool:
        return self.member_id == other.member_id and self.roles == other.roles

    def __hash__(self) -> int:
        return hash((self.member_id, self.roles))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(" \
               f"member_id={repr(self.member_id)}, " \
               f"roles={repr(self.roles)})"


class AuthContext:
    """
    The full API context of the logged-in `Member` and session used to find them.
    """

    def __init__(self, requester: Member, session: Session):
        self.requester = requester
        self.session = session
        self.az = Authorization(
            requester.id,
            [AzRole(r.role, r.committee_id) for r in requester.roles],
        )
