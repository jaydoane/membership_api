from config.lib import from_env, throw

ENCRYPT_KEY: str = from_env.get_str('ENCRYPT_KEY', throw)
