from datetime import datetime, timedelta
from membership.database.base import engine, metadata, Session
from membership.database.models import Member, Election, Vote, EligibleVoter
from membership.web.submit_vote_handler import handle_vote, VotingError
from uuid import uuid4


class TestSubmitVoteHandler:
    @classmethod
    def setup_class(cls):
        metadata.create_all(engine)

    def setup_method(self, method):
        self.session = Session()
        self.election = Election(name=str(uuid4()), number_winners=1)

        self.session.add(self.election)
        self.m = Member()
        self.session.add(self.m)
        self.session.commit()

        self.eligible_voter = EligibleVoter(member_id=self.m.id, election_id=self.election.id)
        self.session.add(self.eligible_voter)
        self.session.commit()

    def test_sets_voted_flag(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        result = handle_vote(self.m, self.session, self.election.id, rankings)
        assert type(result) is not VotingError

        actual = self.session.query(EligibleVoter) \
            .filter_by(member_id=self.m.id, election_id=self.election.id) \
            .one_or_none()
        assert actual.voted is True

    def test_creates_vote(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        vote_key = handle_vote(self.m, self.session, self.election.id, rankings)
        actual = self.session.query(Vote).filter_by(vote_key=vote_key) \
            .one_or_none()

        assert actual.election_id == self.election.id

    def test_creates_ranking(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        vote_key = handle_vote(self.m, self.session, self.election.id, rankings)
        actual = self.session.query(Vote).filter_by(vote_key=vote_key) \
            .one_or_none().ranking

        ranking1 = actual[0]
        assert ranking1.candidate_id == 'Donald Duck'
        assert ranking1.rank == 0
        ranking2 = actual[1]
        assert ranking2.candidate_id == 'Daffy Duck'
        assert ranking2.rank == 1

    def test_election_finalized(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        self.election.status = 'final'
        self.session.commit()
        actual = handle_vote(self.m, self.session, self.election.id, rankings)
        assert actual == VotingError.TOO_LATE

    def test_election_too_early(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        self.election.voting_begins_epoch_millis = \
            int((datetime.now() + timedelta(seconds=5)).timestamp() * 1000)
        self.session.commit()
        actual = handle_vote(self.m, self.session, self.election.id, rankings)
        assert actual == VotingError.TOO_EARLY

    def test_election_too_late(self):
        rankings = ['Donald Duck', 'Daffy Duck']
        self.election.voting_ends_epoch_millis = \
            int((datetime.now() - timedelta(seconds=5)).timestamp() * 1000)
        self.session.commit()
        actual = handle_vote(self.m, self.session, self.election.id, rankings)
        assert actual == VotingError.TOO_LATE

    def test_election_ineligible(self):
        self.session.delete(self.eligible_voter)
        self.session.commit()
        rankings = ['Donald Duck', 'Daffy Duck']
        actual = handle_vote(self.m, self.session, self.election.id, rankings)
        assert actual == VotingError.INELIGIBLE

    def test_election_already_voted(self):
        self.eligible_voter.voted = True
        self.session.commit()
        rankings = ['Donald Duck', 'Daffy Duck']
        actual = handle_vote(self.m, self.session, self.election.id, rankings)
        assert actual == VotingError.ALREADY_VOTED
