import json
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME, SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import InterestTopic, Meeting, Member, Role
from membership.models.authz import AuthContext
from membership.services import AttendeeService, InterestTopicsService, MemberService
from membership.repos import MeetingRepo
from membership.web.base_app import app
from tests.flask_utils import get_json, post_json


interest_topics_service = InterestTopicsService
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService(meetings=meeting_repository)
member_service = MemberService(attendee_service)


class TestEmailTopics:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True
        session = Session()
        # set up for auth
        admin_member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
        )
        session.add(admin_member)
        role = Role(
            member=admin_member,
            role='admin',
        )
        session.add(role)
        topic1 = InterestTopic(name='SocFem')
        session.add(topic1)
        topic2 = InterestTopic(name='Tech')
        session.add(topic2)
        session.commit()
        self.session = session

    def teardown(self):
        metadata.drop_all(engine)

    def test_list_topics(self):
        response = get_json(self.app, '/email_topics')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [
            {
                'id': 1,
                'name': 'SocFem',
            },
            {
                'id': 2,
                'name': 'Tech',
            }
        ]

    def test_add_interests_existing_member(self):
        payload = {
            'topics': 'SocFem, Tech, Housing',
            'email': SUPER_USER_EMAIL,
        }
        response = post_json(self.app, '/email_topics/add_member', payload)
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email(SUPER_USER_EMAIL, session)
        assert member is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert len(topics) == 3

    def test_add_interests_new_member(self):
        payload = {
            'topics': 'SocFem, Tech, Housing',
            'email': ' debs1855@gmail.com',
            'first_name': 'Eugene ',
            'last_name': 'Debs',
        }
        response = post_json(self.app, '/email_topics/add_member', payload)
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email('debs1855@gmail.com', session)
        assert member is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert len(topics) == 3

    def test_add_interests_no_email(self):
        payload = {
            'topics': 'Tech',
            'email': '',
            'first_name': 'Eugene',
            'last_name': 'Debs',
        }
        response = post_json(self.app, '/email_topics/add_member', payload)
        assert response.status_code == 400

    def test_add_interests_new_member_empty_topics(self):
        payload = {
            'topics': '',
            'email': 'debs1855@gmail.com',
            'first_name': 'Eugene',
            'last_name': 'Debs',
        }
        response = post_json(self.app, '/email_topics/add_member', payload)
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email('debs1855@gmail.com', session)
        assert member is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert topics == []
