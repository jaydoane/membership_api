import pytest
from hypothesis import given
from hypothesis.strategies import booleans, dictionaries, text, floats, integers, lists, one_of, \
    recursive, none, sampled_from
from typing import Dict, List, Union

from membership.util.encrypt import create_key, Codec, Decoder, Encoder, encrypt, JsonLike, \
    key_size, parse


class GenJsonLike:
    strings = text()
    numbers = integers() | floats(allow_nan=False)  # nan is not equal to itself
    primitives = one_of([
        text(),
        booleans(),
        numbers,
        strings,
        none(),
    ])
    values = recursive(
        primitives,
        lambda children: lists(children) | dictionaries(text(), children),
        max_leaves=50,
    )
    key_sizes = sampled_from([16, 32])


@pytest.mark.slow
class TestEncrypt:

    @given(GenJsonLike.strings)
    def test_decrypt_inverts_encrypt_text(self, anystr: str):
        assert encrypt(anystr).decrypt() == anystr

    @given(booleans())
    def test_decrypt_inverts_encrypt_bools(self, anybool: bool):
        assert encrypt(anybool).decrypt() == anybool

    @given(GenJsonLike.numbers)
    def test_decrypt_inverts_encrypt_numbers(self, anyfloat: Union[float, int]):
        assert encrypt(anyfloat).decrypt() == anyfloat

    @given(integers())
    def test_decrypt_inverts_encrypt_shallow(self, anyint: int):
        assert encrypt(anyint).decrypt() == anyint

    @given(GenJsonLike.values)
    def test_decrypt_inverts_encrypt_json(self, anyjson: JsonLike):
        assert encrypt(anyjson).decrypt() == anyjson

    @given(integers(0, 256))
    def test_key_size_measures_key_len_in_bytes(self, size: int):
        assert key_size(create_key(size, validate=False)) == size

    @given(GenJsonLike.key_sizes, GenJsonLike.values)
    def test_parse_inverts_serialized(self, encrypt_key_size: int, data: JsonLike):
        key = create_key(encrypt_key_size)
        encrypted = encrypt(data, key=key)
        parsed = parse(encrypted.serialized, key=key)
        assert parsed.nonce == encrypted.nonce
        assert parsed.mac == encrypted.mac
        assert parsed.ciphertext == encrypted.ciphertext
        decrypted = parsed.decrypt()
        assert decrypted == data

    @given(GenJsonLike.key_sizes, GenJsonLike.values)
    def test_repeat_encrypt_uniqueness(self, encrypt_key_size: int, data: JsonLike):
        key = create_key(encrypt_key_size)
        e1 = encrypt(data, key=key)
        e2 = encrypt(data, key=key)
        assert e1.nonce != e2.nonce
        assert e1.mac != e2.mac
        assert e1.ciphertext != e2.ciphertext
        assert e1.serialized != e2.serialized  # we could assume via deduction, but might as well

    @given(GenJsonLike.key_sizes, GenJsonLike.values)
    def test_encrypted_eq_raises_type_error(self, encrypt_key_size: int, data: JsonLike):
        key = create_key(encrypt_key_size)
        encrypted = encrypt(data, key=key)
        with pytest.raises(TypeError) as e_info:
            assert encrypted == encrypted
        assert "Encrypted values should never be used for comparison" in str(e_info)

    @given(GenJsonLike.key_sizes, GenJsonLike.values)
    def test_encrypted_hash_raises_type_error(self, encrypt_key_size: int, data: JsonLike):
        key = create_key(encrypt_key_size)
        encrypted = encrypt(data, key=key)
        with pytest.raises(TypeError) as e_info:
            assert hash(encrypted) == hash(encrypted)
        assert "unhashable type" in str(e_info)


class TestCodec:

    value = 'test'
    test_decoder = Decoder('D', lambda _: TestCodec.value)
    test_encoder = Encoder('E', lambda _: TestCodec.value.encode())
    test_codec = Codec('C', test_decoder, test_encoder)
    json_list_str_codec: Codec[List[str]] = Codec.json('List[str]')
    json_obj_codec: Codec[Dict[str, JsonLike]] = Codec.obj()
    str_codec = Codec.json(str)

    def test_codec_custom_encoding(self):
        assert self.test_codec.type_name == 'C'
        assert self.test_codec.encode('anything').decode() == self.value
        assert self.test_codec.decode('anything'.encode()) == self.value

    def test_codec_str(self):
        assert str(self.test_codec) == "Codec[C]"

    def test_decoder_custom_encoding(self):
        assert self.test_decoder.type_name == 'D'
        assert self.test_decoder.decode('anything'.encode()) == self.value

    def test_decoder_str(self):
        assert str(self.test_decoder) == "Decoder[D]"

    def test_encoder_custom_encoding(self):
        assert self.test_encoder.type_name == 'E'
        assert self.test_encoder.encode('anything').decode() == self.value

    def test_encoder_str(self):
        assert str(self.test_encoder) == "Encoder[E]"

    @given(GenJsonLike.values)
    def test_json_codec_decode_inverts_encode(self, value):
        assert self.str_codec.decode(self.str_codec.encode(value)) == value

    def test_str_codec_str(self):
        assert str(self.str_codec) == "Codec[str]"

    def test_json_list_str_codec(self):
        serialized = self.json_list_str_codec.encode(['1', '2', '3'])
        assert serialized == '["1", "2", "3"]'.encode()

    def test_json_list_str_codec_str(self):
        assert str(self.json_list_str_codec) == "Codec[List[str]]"

    def test_json_obj_codec(self):
        serialized = self.json_obj_codec.encode({
            'key': ['1', '2', '3']
        })
        assert serialized == '{"key": ["1", "2", "3"]}'.encode()

    def test_json_obj_codec_str(self):
        assert str(self.json_obj_codec) == "Codec[Dict[str, JsonLike]]"
