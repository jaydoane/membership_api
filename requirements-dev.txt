-r requirements.txt
flake8==3.4.1
httpretty==0.8.14
hypothesis==3.11.3
pytest-watch==4.1.0
pytest==2.9.2
python-dotenv==0.6.4
yapf==0.11.0
pytest-cov==2.5.1
